/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.benyapa.databaseproject.service;

import com.benyapa.databaseproject.dao.UserDao;
import com.benyapa.databaseproject.model.User;

/**
 *
 * @author bwstx
 */
public class UserService {
    public User login(String name, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByName(name);
        if(user!=null && user.getPassword().equals(password)) {
            return user;
        }
        return null;
    }
    
}
